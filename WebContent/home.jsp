<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<title>User Movie Home</title>
<body>
	<div class="container">
	<div class="row top-buffer">
	<c:forEach var="movie" items="${movies}" varStatus="movieCounter">
		<c:choose> 
			<c:when test="${movieCounter.count == 1}">
				<div class="col-md-3 col-sm-12 box-layout">
			</c:when>
			<c:otherwise>
				<div class="col-md-3 col-md-offset-1 col-md-push-1 col-sm-12 top-buffer box-layout">
			</c:otherwise>
		</c:choose>
            <h2 class="thick"><c:out value="${movie.title}" /></h2>
            <img src="${movie.img}" alt="${movie.title}" class="pull-center img-responsive img-rounded">
            <p class="color-white"><c:out value="${movie.description}" /></p>
            <p class="thick">Rating: </p><p class="color-white"><c:out value="${movie.rating}" /></p>
            <p class="thick">Rating Description: </p><p class="color-white"><c:out value="${movie.ratingDesc}" /></p>
            <p class="thick">Format: </p><p class="color-white"><c:out value="${movie.format}" /></p>
            <p>
	            <a href="${movie.wiki}" target="_blank">
                <button type="button" class="btn btn-default btn-lg">
					More »
                </button>
              </a>
            </p>
         </div>
	</c:forEach>
	</div>
	</div>
</body>
</html>