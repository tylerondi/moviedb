package movie;

public class MovieBean implements java.io.Serializable {
	private static final long serialVersionUID = 2748376517277345500L;
	
	private String title;
	private String description;
	private String rating;
	private String ratingDesc;
	private String format;
	private String img;
	private String wiki;
	
	public MovieBean() {
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getRating() {
		return rating;
	}
	
	public String getRatingDesc() {
		return ratingDesc;
	}
	
	public String getFormat() {
		return format;
	}
	
	public String getImg() {
		return img;
	}
	
	public String getWiki() {
		return wiki;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setRating(String ratingID) {
		this.rating = ratingID;
	}
	
	public void setRatingDesc(String ratingDesc) {
		this.ratingDesc = ratingDesc;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	public void setImg(String img) {
		this.img = img;
	}
	
	public void setWiki(String wiki) {
		this.wiki = wiki;
	}
}
