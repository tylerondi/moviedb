package movie.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import movie.MovieBean;

public class DisplayMoviesModel {
	public static MovieBean[] getMovies(String username) {
		MovieBean[] moviesInit = new MovieBean[getMovieCount(username)];
		MovieBean[] movies = getMovies(username, moviesInit);
		
		return movies;
	}
	
	private static int getMovieCount(String username) {
		int movieCount = 0;
		
		Connection connection = DatabaseConnectionFactory.getInstance().getDatabaseConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		if (connection != null) {
			try {
				statement = connection.prepareStatement("SELECT count(*) AS \'count\'\n" +
														"FROM user u, user_movie um, rating r, movie m\n" +
														"WHERE u.id = um.user_id \n" +
														        "AND r.id = m.rating_id \n" +
														        "AND m.id = um.movie_id \n" +
														        "AND u.user_name = ?;");
				statement.setString(1, username);
				
				rs = statement.executeQuery();
				if (rs != null && !rs.isClosed() && rs.next()) {
					movieCount = rs.getInt("count");
				}
			} catch (SQLException e) {
				System.err.println("Error retrieving movies from database.");
				e.printStackTrace();
			} finally {
				try {
					statement.close();
					rs.close();
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return movieCount;
	}
	
	private static MovieBean[] getMovies(String username, MovieBean[] movies) {
		Connection connection = DatabaseConnectionFactory.getInstance().getDatabaseConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		if (connection != null) {
			try {
				statement = connection.prepareStatement("SELECT m.title, m.description, r.rating, m.rating_description, um.movie_format, m.img, m.wiki\n" +
														"FROM user u, user_movie um, rating r, movie m\n" +
														"WHERE u.id = um.user_id \n" +
														        "AND r.id = m.rating_id \n" +
														        "AND m.id = um.movie_id \n" +
														        "AND u.user_name = ? \n" +
														"ORDER BY m.title, um.movie_format;");
				statement.setString(1, username);
				
				rs = statement.executeQuery();
				for (int i = 0; i < movies.length; i++) {
					if (rs != null && !rs.isClosed() && rs.next()) {
						movies[i] = new MovieBean();
						movies[i].setTitle(rs.getString("title"));
						movies[i].setDescription(rs.getString("description"));
						movies[i].setRating(rs.getString("rating"));
						movies[i].setRatingDesc(rs.getString("rating_description"));
						movies[i].setFormat(rs.getString("movie_format"));
						movies[i].setImg(rs.getString("img"));
						movies[i].setWiki(rs.getString("wiki"));
					}
				}
			} catch (SQLException e) {
				System.err.println("Error retrieving movies from database.");
				e.printStackTrace();
			} finally {
				try {
					statement.close();
					rs.close();
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return movies;
	}
}
