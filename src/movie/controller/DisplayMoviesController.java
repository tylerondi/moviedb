package movie.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import movie.model.DisplayMoviesModel;
import movie.MovieBean;

public class DisplayMoviesController extends HttpServlet {
	private static final long serialVersionUID = -4810875133474431401L;

	@Override
	public void doGet(HttpServletRequest request, 
						HttpServletResponse response) 
						throws ServletException, IOException {
		String username = request.getParameter("username");
		MovieBean[] movieBeans = DisplayMoviesModel.getMovies(username);
		
		request.setAttribute("movies", movieBeans);
		getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
